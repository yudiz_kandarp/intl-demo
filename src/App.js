import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Header, Language } from './Components'
import GlobalContext from './Context/GlobalContext'
import { Home, About, ContactUs } from './Pages'

function App() {
	return (
		<GlobalContext>
			<BrowserRouter>
				<Header />
				<Routes>
					<Route path='/' element={<Home />} />
					<Route path='/about' element={<About />} />
					<Route path='/contactus' element={<ContactUs />} />
				</Routes>
				<Language />
			</BrowserRouter>
		</GlobalContext>
	)
}

export default App
