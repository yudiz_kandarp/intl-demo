import React from 'react'
import { NavLink } from 'react-router-dom'
import Translate from '../../i18n/Translate'
import '../../Styles/header.scss'
function Header() {
	return (
		<header>
			<nav>
				<NavLink to='/'>Tzaaa</NavLink>
				<ul>
					<li>
						<NavLink
							to='/'
							style={({ isActive }) => ({
								textDecoration: isActive ? 'underline' : 'none',
								textDecorationThickness: isActive ? '4px' : '0px',
							})}
						>
							{Translate('homeHead')}
						</NavLink>
					</li>
					<li>
						<NavLink
							to='/about'
							style={({ isActive }) => ({
								textDecoration: isActive ? 'underline' : 'none',
								textDecorationThickness: isActive ? '4px' : '0px',
							})}
						>
							{Translate('aboutHead')}
						</NavLink>
					</li>
					<li>
						<NavLink
							to='/contactus'
							style={({ isActive }) => ({
								textDecoration: isActive ? 'underline' : 'none',
								textDecorationThickness: isActive ? '4px' : '0px',
							})}
						>
							{Translate('contactusHead')}
						</NavLink>
					</li>
				</ul>
			</nav>
		</header>
	)
}

export default Header
