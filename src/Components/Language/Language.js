import React from 'react'
import { useGlobal } from '../../Context/GlobalContext'
import { LOCALES } from '../../i18n'
import '../../Styles/language.scss'
function Language() {
	const { locale, setLocale } = useGlobal()

	return (
		<select
			className='language'
			onChange={(e) => {
				setLocale(e.target.value)
				localStorage.setItem('lang', e.target.value)
			}}
			value={locale}
		>
			<option value={LOCALES.ENGLISH}>English 🇺🇸</option>
			<option value={LOCALES.GERMAN}>German 🇩🇪</option>
			<option value={LOCALES.FRENCH}>French 🇫🇷</option>
			<option value={LOCALES.JAPANESE}>Japanese 🇯🇵</option>
			<option value={LOCALES.ITALIAN}>Italian 🇮🇹</option>
		</select>
	)
}

export default Language
