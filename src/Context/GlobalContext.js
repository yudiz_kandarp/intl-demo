import React, { createContext, useContext, useState } from 'react'
import { I18nProvider, LOCALES } from '../i18n'
const Global = createContext()
function GlobalContext({ children }) {
	const [locale, setLocale] = useState(
		localStorage.getItem('lang') || LOCALES.ENGLISH
	)
	return (
		<Global.Provider value={{ locale, setLocale }}>
			<I18nProvider locale={locale}>{children}</I18nProvider>
		</Global.Provider>
	)
}
export function useGlobal() {
	return useContext(Global)
}
export default GlobalContext
