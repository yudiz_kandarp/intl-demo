import React from 'react'
import '../Styles/about.scss'
import Translate from '../i18n/Translate'
import { FormattedMessage, FormattedNumber, FormattedTime } from 'react-intl'
function About() {
	return (
		<div className='about_section'>
			<div className='about_container'>
				<div className='about title'>{Translate('about')}</div>
				<div className='about'>{Translate('aboutBody')}</div>
				<div className='about title'>
					<FormattedTime
						value={new Date()}
						day='numeric'
						month='long'
						year='2-digit'
					/>
				</div>

				<div className='about title'>
					{/* <FormattedMessage id='ratio'>
						{(d) => {
							return (
								<FormattedNumber
									value={d * 30}
									style='currency'
									currency={data}
								/>
							)
						}}
					</FormattedMessage> */}
					<FormattedMessage id='curr'>
						{(data) => {
							return (
								<FormattedMessage id='ratio'>
									{(d) => {
										return (
											<FormattedNumber
												value={d * 30}
												// eslint-disable-next-line react/style-prop-object
												style='currency'
												currency={data}
											/>
										)
									}}
								</FormattedMessage>
							)
						}}
					</FormattedMessage>
				</div>
			</div>
		</div>
	)
}

export default About
