import React from 'react'
import '../Styles/home.scss'
import Translate from '../i18n/Translate'
function Home() {
	return (
		<section>
			
			<div className='hero_section'>
			{Translate('title')}
			</div>
		</section>
	)
}

export default Home
