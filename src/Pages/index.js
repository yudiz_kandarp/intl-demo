import About from './About'
import ContactUs from './ContactUs'
import Home from './Home'

export { Home, About, ContactUs }
