import React from 'react'
import { FormattedMessage } from 'react-intl'

import '../Styles/contact.scss'
function ContactUs() {
	return (
		<div className='contact_section'>
			<div className='contact_container'>
				<form
					onSubmit={(e) => {
						e.preventDefault()
						alert('thank you for reaching out.')
					}}
				>
					<FormattedMessage id='contactemail'>
						{(formattedValue) => {
							return (
								<input
									className='contact_email_input'
									type='email'
									placeholder={formattedValue}
								/>
							)
						}}
					</FormattedMessage>
					<FormattedMessage id='writeHere'>
						{(formattedValue) => {
							return (
								<textarea
									className='contact_email_input'
									placeholder={formattedValue}
								/>
							)
						}}
					</FormattedMessage>
					<button> send</button>
				</form>
			</div>
		</div>
	)
}

export default ContactUs
