import { LOCALES } from '../locales'

const data = {
	[LOCALES.GERMAN]: {
		title: 'Glück kann man nicht kaufen, aber Pizza',
		about: 'Pizza und Freunde sind alles, was wir im Leben brauchen',
		contactus: 'Denn wer kann ohne Pizza leben, oder?',
		homeHead: 'Zuhause',
		aboutHead: 'Über',
		contactusHead: 'kontaktiere uns',
		aboutTitle: 'Was wir brauchen - Mehr Auswahl. Mehr Spaß.',
		aboutBody:
			"Egal in welcher Situation, Pizza hilft immer. Besonders eine Pizza, die Ihnen die Freiheit gibt, Ihre Beläge zu wählen - von Paneer, knackiger Paprika, Zwiebel, gegrillten Pilzen, goldenem Mais, schwarzen Oliven, frischen Tomaten, rotem Paprika, Jalapeno, Paneer Tikka und extra Käse bis hin zu nicht-vegetarischen Belägen wie z Paprika-Barbeque-Huhn, Peri-Peri-Huhn, gegrilltes Hähnchenfleisch, Hähnchenwurst oder Hähnchen-Tikka – die Möglichkeiten sind nahezu endlos, alles und alles, was Sie sich dazu vorstellen können, auf der Kruste Ihrer Wahl – Neue handgeworfene Kruste, Weizen dünner Boden, geplatzter Käseboden, klassischer handgeworfener Boden oder eine frische Pfannenpizza. Denn genau wie eine Pizza versteht Domino's Sie. Und um den Monatsend-Blues zu verscheuchen, während Sie Ihre Bestellung bei unserem Pizza-Restaurant aufgeben, sehen Sie sich die Pizzas für jeden Tag von Pizza Mania Combos mit täglichem Vorteilsangebot von 2 Pizzen ab an nur jeweils 99 ₹ (normale Größe) und 199 ₹ (mittlere Größe).",
		contactemail: 'Email',
		writeHere: 'hier schreiben',
		curr: 'EUR',
		ratio: '0.91',
	},
}
export default data
