import { LOCALES } from '../locales'
const data = {
	[LOCALES.ITALIAN]: {
		title: 'non puoi comprare la felicità ma puoi comprare la Pizza',
		about: 'Pizza e amici sono tutto ciò di cui abbiamo bisogno nella vita',
		contactus: 'Perché chi può vivere senza pizza, giusto?',
		homeHead: 'casa',
		aboutHead: 'di',
		contactusHead: 'Contattaci',
		aboutTitle: 'Di cosa abbiamo bisogno: più scelta. Più divertimento.',
		aboutBody:
			"Non importa quale sia la situazione, la pizza aiuta sempre. Soprattutto una pizza che ti dà la libertà di scegliere i tuoi condimenti - da paneer, peperoni croccanti, cipolla, funghi grigliati, mais dorato, olive nere, pomodoro fresco, paprika rossa, jalapeno, paneer tikka e formaggio extra a condimenti non vegetali come pollo al barbecue con peperoni, pollo peri-peri, fetta di pollo alla griglia, salsiccia di pollo o tikka di pollo: le opzioni sono quasi infinite, qualsiasi cosa tu possa pensare anche a quello sopra la crosta di tua scelta - Nuova crosta lanciata a mano, grano crosta sottile, crosta di formaggio, classica crosta girata a mano o una pizza fresca in teglia. Perché proprio come una pizza, Domino's ti capisce. E per scacciare quei blues di fine mese mentre effettui l'ordine con il nostro ristorante pizzeria, dai un'occhiata alle pizze di tutti i giorni dalle combo pizza mania con l'offerta giornaliera di 2 pizze a partire da solo ₹ 99 (taglia normale) e ₹ 199 (taglia media) ciascuno.",
		contactemail: 'e-mail',
		writeHere: 'scrivere qui',
		curr: 'EUR',
		ratio: '0.91',
	},
}
export default data
