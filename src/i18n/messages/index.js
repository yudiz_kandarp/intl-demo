import deDE from './de-DE'
import enUS from './en-US'
import frCA from './fr-CA'
import jpJP from './jp-JP'
import itIT from './it-IT'
const data = { ...deDE, ...enUS, ...frCA, ...jpJP, ...itIT }
export default data
