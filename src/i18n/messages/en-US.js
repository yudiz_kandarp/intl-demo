import { LOCALES } from '../locales'
const data = {
	[LOCALES.ENGLISH]: {
		title: "you can't buy happiness but you can buy Pizza ",
		about: 'Pizza and friends are all we need in life',
		contactus: 'Because who can live without pizza, right?',
		homeHead: 'home',
		aboutHead: 'about',
		contactusHead: 'contact us',
		aboutTitle: 'What We Need - More Choice. More Fun.',
		aboutBody:
			"No matter what the situation, pizza always helps. Especially a pizza that gives you the freedom to choose your toppings - from paneer, crisp capsicum, onion, grilled mushroom, golden corn, black olives, fresh tomato, red paprika, jalapeno, paneer tikka and extra cheese to non-veg toppings such as pepper barbeque chicken, peri-peri chicken, grilled chicken rasher, chicken sausage or chicken tikka- the options are almost endless, anything and everything you can think of that too on top of the crust of your choice - New hand-tossed crust, wheat thin crust, cheese burst crust, classic hand-tossed crust or a fresh pan pizza. Because just like a pizza, Domino's understands you.And to shoo away those end-of-the-month blues while placing your order with our pizza restaurant, check out the everyday pizzas from pizza mania combos with everyday value offer of 2 pizzas starting at just ₹99 (regular size) and ₹199 (medium size) each.",
		contactemail: 'Email',
		writeHere: 'Write Here',
		curr: 'USD',
		ratio: '1',
	},
}
export default data
