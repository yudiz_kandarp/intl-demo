import { LOCALES } from '../locales'
const data = {
	[LOCALES.FRENCH]: {
		title: 'tu ne peux pas acheter le bonheur mais tu peux acheter de la pizza',
		about:
			'La pizza et les amis sont tout ce dont nous avons besoin dans la vie',
		contactus: "Car qui peut vivre sans pizza, n'est-ce pas ?'",
		homeHead: 'domicile',
		aboutHead: 'sur',
		contactusHead: 'Nous contacter',
		aboutTitle: 'Ce dont nous avons besoin - Plus de choix. Plus amusant.',
		aboutBody:
			"Quelle que soit la situation, la pizza aide toujours. Surtout une pizza qui vous donne la liberté de choisir vos garnitures - du paneer, du poivron croustillant, de l'oignon, des champignons grillés, du maïs doré, des olives noires, de la tomate fraîche, du paprika rouge, du jalapeno, du paneer tikka et du fromage supplémentaire aux garnitures non végétariennes telles que poulet barbecue au poivre, poulet péri-péri, tranche de poulet grillé, saucisse de poulet ou poulet tikka - les options sont presque infinies, tout ce à quoi vous pouvez penser aussi sur la croûte de votre choix - Nouvelle croûte à la main, blé une croûte mince, une croûte éclatée au fromage, une croûte classique lancée à la main ou une pizza à la poêle fraîche. Parce que tout comme une pizza, Domino's vous comprend.Et pour chasser le blues de fin de mois en passant votre commande auprès de notre pizzeria, découvrez les pizzas du quotidien des combos pizza mania avec une offre quotidienne de 2 pizzas à partir de seulement 99 ₹ (taille normale) et 199 ₹ (taille moyenne) chacun.",
		contactemail: 'E-mail',
		writeHere: 'Ecrire ici',
		curr: 'EUR',
		ratio: '0.91',
	},
}
export default data
